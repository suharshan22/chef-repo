file_cache_path "/home/directfn/devops/chef-solo"
cookbook_path [
	"/home/directfn/devops/chef-repo/cookbooks",
	"/home/directfn/.berkshelf/cookbooks"
	]
data_bag_path "/home/directfn/devops/chef-repo/data_bags"
environment "development"
environment_path "/home/directfn/devops/chef-repo/environments"
