#
# Cookbook Name:: mfs-mre
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

mre_home = "#{node['app_home']}/mre"

# Download tar file
remote_file "/tmp/#{node['mfs-mre']['filename']}" do
  source "#{node['package_url']}/#{node['mfs-mre']['filename']}"
end

directory mre_home do
  mode "0755"
  action :create
end

# Extract MRE
execute "extract_mre" do
  command "tar -zxf /tmp/#{node['mfs-mre']['filename']} -C #{mre_home}"
end 

# Create config.ini file
template "#{mre_home}/config/config.ini" do
	source "config.ini.erb"
	variables ({
		:cm_id => node['mfs-mre']['cm_id'],
		:price_server_ips => node['mfs-mre']['price_server_ips'].join(','),
		:jms_ip => node['mfs-mre']['jms_ip'],
		:jms_port => node['mfs-mre']['jms_port'],
		:db_host => node['mfs-mre']['db_host'],
		:db_sid => node['mfs-mre']['db_sid'],
		:oms_ip => node['mfs-mre']['oms_ip']
	})
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{mre_home}"
  command "chmod -Rf 777 #{mre_home}"
  action :run
end

# Run application
#execute "run_mre" do
# command "/bin/bash #{mre_home}/start-service.sh"
# action :run
#end
