default['mfs-mre']['filename'] = 'MRE.tgz'
default['mfs-mre']['cm_id'] = 'MRE_MBS'
default['mfs-mre']['price_server_ips'] = [
	'172.17.240.228',
	'172.17.240.229'
]
default['mfs-mre']['jms_ip'] = '172.18.31.81'
default['mfs-mre']['jms_port'] = '1099'
default['mfs-mre']['db_host'] = 'mfsbo_101.mfsnet.io'
default['mfs-mre']['db_sid'] = 'OMSBO'
default['mfs-mre']['oms_ip'] = '172.18.31.81'
