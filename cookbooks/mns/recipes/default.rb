#
# Cookbook Name:: mns
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

mns_home = "#{node['app_home']}/mns"

# Download source
remote_file "/tmp/#{node['mns']['filename']}" do
  source "#{node['package_url']}/#{node['mns']['filename']}"
  user "directfn"
end

directory mns_home do
  mode "0755"
  action :create
end

# Extract application
execute "extract_mns" do
  command "tar -zxf /tmp/#{node['mns']['filename']} -C #{mns_home}"
end

# Populate Settings.xml from template
template "#{mns_home}/config/settings.ini" do
  source "settings.ini.erb"
	owner "directfn"
  variables ({
    :ora_server => node['mns']['ora_server'],
    :ora_sid => node['mns']['ora_sid']
  })
end

# Load jobs from databag
jobs = data_bag('mns_jobs')

# Iterate over each job
job_content = ''
jobs.each do |job_item|
	job = data_bag_item('mns_jobs', job_item)
	
	all_content = "<Job>\n"

	# Iterate over each key/value pair and fill job's body
	job.each do |key,value|
		all_content << <<"EOF"
			<#{key}>#{value}</#{key}>
EOF
	end

	all_content << "</Job>\n\n"
	job_content << all_content	
end

# Create job-schedule-config.xml
template "#{mns_home}/config/job-schedule-config.xml" do
	source "job-schedule-config.xml.erb"
	owner "directfn"
	variables ({
		:jobs => job_content
	})
end

# Create start.sh
template "#{mns_home}/start.sh" do
	source "start.sh.erb"
	owner "directfn"
	variables ({
		:java_path => "#{node['mns']['java_path']}"
	})
end

# Issue permissions and ownership change
execute "change_mns_permission" do
  command "chown -Rf directfn:directfn #{mns_home}"
  command "chmod -Rf 777 #{mns_home}"
  action :run
end

# Start app
execute "start_mns" do
	command "/bin/bash #{mns_home}/start.sh"
end
