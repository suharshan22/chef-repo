#
# Cookbook Name:: mfs-jetty
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

node.set['jetty']['download']  = "#{node['jetty']['home']}/JETTY7.tar.gz"
node.set['jetty']['extracted'] = "#{node['jetty']['home']}/JETTY7"
node.set['jetty']['app_name'] = 'JETTY7'
node.set['jetty']['link'] = "#{node['package_url']}/#{node['jetty']['package_name']}"

# Create Jetty home
[node['jetty']['extracted'], "#{node['jetty']['extracted']}/temp"].each do |d|
	directory d do
		owner node['jetty']['user']
		group node['jetty']['group']
		recursive true
		mode  '755'
	end
end

# Download jetty archive 
remote_file node['jetty']['download'] do
  source   node['jetty']['link']
  checksum node['jetty']['checksum']
  mode     0644
end

# Extract Jetty
ruby_block 'Extract Jetty' do
  block do
    Chef::Log.info "Extracting Jetty archive #{node['jetty']['download']} into #{node['jetty']['home']}"
    `tar xzf #{node['jetty']['download']} -C #{node['jetty']['extracted']}`
    raise "Failed to extract Jetty package" unless File.exists?(node['jetty']['extracted'])
  end

  action :create

#  not_if do
#    File.exists?(node['jetty']['extracted'])
#  end
end

# Merge MFS changes
bash "merge_jetty_changes" do
  cwd node['jetty']['extracted']
  code <<-EOH
  git init
  git remote add origin #{node['jetty']['git_repo']}
  git fetch
  git checkout -t origin/master -f
  EOH
  not_if { ::Dir.exists?("#{node['jetty']['extracted']}/.git") }
end

# Create jetty.sh
template "#{node['jetty']['extracted']}/bin/jetty.sh" do
	source "jetty.sh.erb"
	variables ({
		:java_home => node['java6_home'],
		:app_name => node['jetty']['app_name'],
		:tmp_dir => node['jetty']['tmp_dir'],
		:min_memory => node['jetty']['min_memory'],
		:max_memory => node['jetty']['max_memory']
	})
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{node['jetty']['extracted']}; chmod -Rf 777 #{node['jetty']['extracted']}"
  action :run
end

# Start jetty
execute "run_jetty" do
	command "#{node['jetty']['extracted']}/bin/jetty.sh start"
  action :run
end
