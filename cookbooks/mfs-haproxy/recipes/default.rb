#
# Cookbook Name:: mfs-haproxy
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'haproxy'
require 'json'

node.default['mfs-haproxy']['frontend_ip'] = node['ipaddress']

template "#{node['haproxy']['conf_dir']}/haproxy.cfg" do
	source "haproxy.cfg.erb"
	mode "0744"
	variables ( node['mfs-haproxy'] ) 
end

file '/tmp/pub' do
	content node.to_json
end
