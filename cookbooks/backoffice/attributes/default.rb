default['backoffice']['base_url'] = 'http://10.1.200.176:9000'
default['backoffice']['home'] = '/home/directfn/app'
default['backoffice']['jdk7_url'] = 'http://10.1.200.176:9000/jdk-7-linux-x64.rpm'
default['backoffice']['java_path'] = '/usr/java'
default['backoffice']['git']['url'] = 'gitlab.mfsnet.io'
default['backoffice']['java6_package'] = 'jdk1.6.0_31.tar.gz'
default['backoffice']['java7_package'] = 'jdk-7-linux-x64.rpm'
