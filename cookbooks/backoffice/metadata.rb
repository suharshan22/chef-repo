name             'backoffice'
maintainer       'DirectFN'
maintainer_email 'p.pubudu@directfn.com'
license          'All rights reserved'
description      'Installs/Configures backoffice'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "openssh"
depends "git"
depends "hostsfile"
depends "ssh_known_hosts"
