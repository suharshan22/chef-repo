#
# Cookbook Name:: backoffice
# Recipe:: java6
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

directory "/usr/java" do
  owner "directfn"
  group "directfn"
  recursive true
  action :create
end

remote_file "#{node['backoffice']['java_path']}/#{node['backoffice']['java7_package']}" do
  source "#{node['package_url']}/#{node['backoffice']['java7_package']}"
end

rpm_package "jdk" do
  source "#{node['package_url']}/#{node['backoffice']['java7_package']}"
	action :install
end
