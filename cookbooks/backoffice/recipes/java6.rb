#
# Cookbook Name:: backoffice
# Recipe:: java6
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

directory "/usr/java" do
  owner "directfn"
  group "directfn"
  recursive true
  action :create
end

remote_file "#{node['backoffice']['java_path']}/#{node['backoffice']['java6_package']}" do
  source "#{node['package_url']}/#{node['backoffice']['java6_package']}"
end

bash "extract_java" do
  cwd node['backoffice']['java_path'] 
  code <<-EOH
  tar -zxf "#{node['backoffice']['java_path']}/jdk1.6.0_31.tar.gz"
	chown -R directfn.directfn #{node['backoffice']['java_path']}
  EOH
end
