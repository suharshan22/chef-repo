
default['jboss-eap']['version'] = "6.1.0"
default['jboss-eap']['install_path'] = '/home/directfn/app'
default['jboss-eap']['symlink'] = 'jboss'
default['jboss-eap']['jboss_home'] = "#{node['jboss-eap']['install_path']}/#{node['jboss-eap']['symlink']}"
default['jboss-eap']['config_dir'] = "#{node['jboss-eap']['jboss_home']}/server/default/conf"
default['jboss-eap']['package_url'] = "#{node['package_url']}/jboss-as-distribution-6.1.0.Final.zip"
default['jboss-eap']['checksum'] = '3426fda6b7066027437de76d02dd5418abc614aec3e21255f8aa704e56a1a291'
default['jboss-eap']['log_dir'] = "#{node['jboss-eap']['jboss_home']}/server/default/log"
default['jboss-eap']['jboss_user'] = 'directfn'
default['jboss-eap']['jboss_group'] = 'directfn'
default['jboss-eap']['admin_user'] = nil
default['jboss-eap']['admin_passwd'] = nil # Note the password has to be >= 8 characters, one numeric, one special
default['jboss-eap']['start_on_boot'] = false
