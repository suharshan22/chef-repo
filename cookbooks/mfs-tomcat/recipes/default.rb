#
# Cookbook Name:: bo-tomcat
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

include_recipe "mfs-tomcat::tomcat"
require 'json'

tomcat_home = "#{node['app_home']}/tomcat"

# DirectFN specific configurations for Tomcat
bash "merge_tomcat_changes" do
  cwd tomcat_home
  code <<-EOH
  git init
  git remote add origin #{node['mfs-tomcat']['git_repo']}
  git fetch
  git checkout -t origin/master -f
	EOH
	not_if { ::Dir.exists?("#{tomcat_home}/.git") }
end

# Create setenv.sh
template "#{tomcat_home}/bin/setenv.sh" do
	source "setenv.sh.erb"
	mode "0755"
	variables ({
		:java_home => node['java6_home'],
		:basedir => tomcat_home, 
    :min_memory_in_mb => node['mfs-tomcat']['min_memory_in_mb'],
    :max_memory_in_mb => node['mfs-tomcat']['max_memory_in_mb']
	})
end

# Iterate through attributes and update WAR files accordingly
node['mfs-tomcat']['file_list'].each do |key,warfilename|

	# Download war file
	remote_file "#{tomcat_home}/webapps/#{warfilename}" do
		mode "0755"
		source "#{node['package_url']}/#{warfilename}"
		action :create_if_missing
	end

	if node['mfs-tomcat'].has_key?(warfilename)
	node['mfs-tomcat']["#{warfilename}"].each do |filename, params|

   	# Create temp directory
   	tmpdir = "/tmp/#{warfilename}/#{File.dirname(filename)}"
   	directory tmpdir do
   	  recursive true
    	 action :create
   	end

	   # Create file from each template
  	 template "/tmp/#{warfilename}/#{filename}" do
    	 source "#{warfilename}/#{File.basename(filename)}.erb"    
	     variables (params)
  	 end

#		pub = "jar -uvf #{node['app_home']}/webapps/#{warfilename} #{filename}"
#		bash "update_war" do
#			cwd "/tmp/#{warfilename}"
#			code <<-EOH
#			#{pub} >> /tmp/pub 2>&1
#			EOH
#		end

	   # Update war file
  	 execute "update_war_file" do
    	 cwd "/tmp/#{warfilename}"
     	command "/usr/bin/jar -uvf #{tomcat_home}/webapps/#{warfilename} #{filename} >> /tmp/pub 2>&1"
	     action :run
  	 end

	end
	end
end

# Issue permissions and ownership change
execute "change_tomcat_permission" do
  command "chown -Rf directfn:directfn #{tomcat_home}/"
  action :run
end

# Start tomcat
unless Dir.exist? "#{tomcat_home}/catalina.pid"
execute "start_tomcat" do
	cwd "#{tomcat_home}/bin"
	command "/bin/bash startup.sh"
	user "directfn"
	action :run
end 
end
