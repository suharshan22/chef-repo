node.override['tomcat-all']['version'] = '6.0.35'
node.override['tomcat-all']['install_directory'] = node['app_home']
include_recipe "tomcat-all"
