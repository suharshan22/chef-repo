default['mfs-tomcat']['git_repo'] = 'https://suharshan22:suharshan2005@bitbucket.org/suharshan22/mfs-tomcat-changes.git.git'
default['mfs-tomcat']['min_memory_in_mb'] = '1024'
default['mfs-tomcat']['max_memory_in_mb'] = '1024'
default['mfs-tomcat']['db_host'] = 'mfsfo_uat.mfsnet.io'
default['mfs-tomcat']['db_sid'] = 'MFSOMS'
default['mfs-tomcat']['db_user'] = 'MFS_FO_OMS'
default['mfs-tomcat']['db_pass'] = 'password'

default['mfs-tomcat']['file_list'] = ''

default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/resources/messages.properties']['site_address'] = 'http://localhost'
default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/resources/messages_ar.properties']['site_address'] = 'http://localhost'
default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/classes/META-INF/NGP-config.xml']['dcp_profile_addr'] = '127.0.0.1'
default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/classes/META-INF/NGP-config.xml']['institution_id'] = '123'
default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/classes/META-INF/NGP-config.xml']['master_account_id'] = '123'
default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/classes/META-INF/NGPURL-config.xml']['comet_url'] = '127.0.0.1'
default['mfs-tomcat']['DUTradeRIA.war']['WEB-INF/classes/META-INF/NGPURL-config.xml']['app_url'] = '127.0.0.1'

default['mfs-tomcat']['DUUserReg.war']['WEB-INF/resources/messages.properties']['master_acc_no'] = '123'
default['mfs-tomcat']['DUUserReg.war']['WEB-INF/resources/messages.properties']['parent_site_address'] = 'http://localhost'
default['mfs-tomcat']['DUUserReg.war']['WEB-INF/resources/messages.properties']['site_address'] = 'http://ldcria-uat.dutrade.com'
default['mfs-tomcat']['DUUserReg.war']['WEB-INF/resources/messages_ar.properties']['parent_site_address'] = 'http://localhost'
default['mfs-tomcat']['DUUserReg.war']['WEB-INF/resources/messages_ar.properties']['site_address'] = 'http://ldcria-uat.dutrade.com'

default['mfs-tomcat']['revenueDataPersistingService.war']['WEB-INF/classes/jdbc.properties']['db_host'] = '172.18.31.81'
default['mfs-tomcat']['revenueDataPersistingService.war']['WEB-INF/classes/jdbc.properties']['db_sid'] = 'mfsuat'
default['mfs-tomcat']['revenueDataPersistingService.war']['WEB-INF/classes/jdbc.properties']['db_user'] = 'mubasher_revenue_data'
default['mfs-tomcat']['revenueDataPersistingService.war']['WEB-INF/classes/jdbc.properties']['db_pass'] = 'password'


