name             'mfs-tomcat'
maintainer       'DirectFN'
maintainer_email 'p.pubudu@directfn.com'
license          'All rights reserved'
description      'Installs/Configures bo-tomcat'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'tomcat-all'
depends 'git'
