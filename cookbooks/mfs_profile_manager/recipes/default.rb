#
# Cookbook Name:: mfs_profile_manager
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

include_recipe "git"
include_recipe "mfs_profile_manager::jboss"
include_recipe "ssh_known_hosts"

directory node['jboss-eap']['jboss_home'] do
  owner "directfn"
  group "directfn"
  recursive true
end

# DirectFN specific additions to Jboss
bash "merge_jboss_changes" do
  cwd node['jboss-eap']['jboss_home']
  code <<-EOH
  git init
  git remote add origin #{node['mfs_profile_manager']['repo_name']}
  git fetch
  git checkout -t origin/master -f
  EOH
  not_if { ::Dir.exists?("#{node['jboss-eap']['jboss_home']}/.git") }
end

# Create oracle-ds.xml
template "#{node['jboss-eap']['jboss_home']}/server/production/deploy/oracle-ds.xml" do
	source "oracle-ds.xml.erb"
	variables ({
		:fo_db_host => node['mfs_profile_manager']['fo_db_host'],
		:fo_db_sid => node['mfs_profile_manager']['fo_db_sid']
	})
end

# Create jboss.log
directory "#{node['jboss-eap']['jboss_home']}/server/production/log" do
	action :create
end

file "#{node['jboss-eap']['jboss_home']}/server/production/log/jboss.log" do
	action :create
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{node['jboss-eap']['jboss_home']}; chmod -Rf 777 #{node['jboss-eap']['jboss_home']}"
  action :run
end

# Start jboss
execute "start_jboss" do
	command "#{node['jboss-eap']['jboss_home']}/bin/jboss_init.sh start"
  action :run
end
