# Cookbook Name:: bo_oms
# Recipe:: jboss
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

node.override['jboss-eap']['admin_user'] = "youradmin"
node.override['jboss-eap']['admin_passwd'] = "ZYxalFHy-7A" # Note the password has to be >= 8 characters, one numeric, one special
node.override['jboss-eap']['start_on_boot'] = true
node.override['jboss-eap']['install_path'] = node['app_home']
node.override['jboss-eap']['symlink'] = "jboss6"
node.override['jboss-eap']['jboss_home'] = "#{node['app_home']}/#{node['jboss-eap']['symlink']}"
node.override['jboss-eap']['config_dir'] = "/etc/jboss-as"
node.override['jboss-eap']['log_dir'] = "/var/log/jboss"
include_recipe "jboss-eap::jboss-as"
