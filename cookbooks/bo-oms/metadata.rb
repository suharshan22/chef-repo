name             'bo-oms'
maintainer       'DirectFN'
maintainer_email 'p.pubudu@directfn.com'
license          'All rights reserved'
description      'Installs/Configures bo-oms'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "jboss-eap", "= 2.1.1"
depends "chef-vault"
depends "ssh_known_hosts"
depends "git"
