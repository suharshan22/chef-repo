#
# Cookbook Name:: bo-oms
# Recipe:: java
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

directory "/usr/java" do
  owner "directfn"
  group "directfn"
	recursive true
  action :create
end

remote_file "#{node['bo_oms']['java_path']}/jdk.tgz" do
	source node['bo_oms']['jdk_url']
end

bash "extract_java" do
	cwd "/usr/java"
	code <<-EOH
	tar -zxf "#{node['bo_oms']['java_path']}/jdk.tgz"
	EOH
	notifies :create, "directory[/usr/java]", :immediately
end

