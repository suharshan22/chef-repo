#
# Cookbook Name:: bo-oms
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

#include_recipe "bo-oms::java"
include_recipe "bo-oms::jboss"
include_recipe "ssh_known_hosts"

# Create ssh key from databag
#include_recipe 'chef-vault'
#directfn_key = ChefVault::Item.load('private_keys', 'directfn')
#file "/home/directfn/.ssh/id_rsa" do
#	user "directfn"
#	mode '0400'
#	content directfn_key['file-content']
#end

directory node['jboss-eap']['jboss_home'] do
	owner "directfn"
	group "directfn"
	recursive true
end

# DirectFN specific one time configurations to jboss
unless Dir.exist? "#{node['jboss-eap']['jboss_home']}/.git"
bash "merge_jboss_changes" do
	cwd node['jboss-eap']['jboss_home']
	code <<-EOH
	git init
	git remote add origin #{node['bo_oms']['git']['repo_name']}
	git fetch
	#git add .
	#git commit -m 'test'
	git checkout -t origin/master -f
	EOH
	#not_if { ::Dir.exists?("#{node['jboss-eap']['jboss_home']}/.git") }
end
end

# Create run.conf file
template "#{node['jboss-eap']['jboss_home']}/bin/run.conf" do
	source "run.conf.erb"
	mode "0755"
	variables ({
		:min_memory_in_mb => node['bo_oms']['min_memory_in_mb'],
		:max_memory_in_mb => node['bo_oms']['max_memory_in_mb']
	})
end

# Create oracle-xa-ds.xml file
template "#{node['jboss-eap']['jboss_home']}/server/default/deploy/oracle-xa-ds.xml" do
	source "oracle-xa-ds.xml.erb"
	variables ({
		:ora_server => node['bo_oms']['ora_server'],
		:ora_sid => node['bo_oms']['ora_sid']
	})
end

# Update startnet.sh
template "#{node['jboss-eap']['jboss_home']}/bin/startnet.sh" do
	source "startnet.sh.erb"
	variables ({
		:private_ip => node['ipaddress']
	})
end

# Add release specific files
include_recipe "bo-oms::oms_release"

execute "change_permission" do
  command "chown -Rf directfn:directfn #{node['jboss-eap']['jboss_home']}; chmod -Rf 777 #{node['jboss-eap']['jboss_home']}"
  action :run
end

# Start OMS
execute "start_oms" do
  cwd "#{node['jboss-eap']['jboss_home']}/bin"
  command "/bin/bash startnet.sh"
  user "directfn"
  action :run
end

# TODO: Wait until OMS starts and open port

