#
# Cookbook Name:: bo-oms
# Recipe:: user_reg
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#


release_file = node['bo-oms']['user_reg']['filename']

# Download war file
remote_file "#{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file}" do
  source "#{node['package_url']}/#{release_file}"
  action :create_if_missing
end

tmp_dir = "/tmp/#{release_file}/WEB-INF"

# Create temp directory
directory "#{tmp_dir}/lib" do
  recursive true
  action :create
end

directory "#{tmp_dir}/resources" do
  recursive true
  action :create
end

# Create web.xml
template "#{tmp_dir}/web.xml" do
	source "user_reg/web.xml.erb"
	variables ({
		:institution_id => node['bo-oms']['user_reg']['institution_id']
	})
end

ipaddress = node['ipaddress']

# Create userreg-services.xml
template "#{tmp_dir}/userreg-services.xml" do
	source "user_reg/userreg-services.xml.erb"
	variables ({
		:bo_oms_ip => ipaddress
	})
end

# Create application.properties
template "#{tmp_dir}/resources/application.properties" do
	source "user_reg/application.properties.erb"
	variables ({
		:master_acc_no => node['bo-oms']['user_reg']['master_acc_no']
	})
end

# Create messages.properties
template "#{tmp_dir}/resources/messages.properties" do
	source "user_reg/messages.properties.erb"
	variables ({
		:institution_home => node['bo-oms']['user_reg']['institution_home']
	})
end

# Create messages_ar.properties
template "#{tmp_dir}/resources/messages_ar.properties" do
	source "user_reg/messages_ar.properties.erb"
	variables ({
		:institution_home => node['bo-oms']['user_reg']['institution_home']
	})
end

# Copy OMS release files
release_files = data_bag_item('bo_oms_releases', node['bo_oms']['release'])

release_files['files'].each do |file|
  remote_file "#{tmp_dir}/lib/#{file}" do
    source "#{node['package_url']}/#{file}"
  end

	execute "copy_#{file}" do
		cwd "/tmp/#{release_file}"
		command "/usr/bin/jar -uf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/lib/#{file} >> /tmp/pub 2>&1"
	end
end

# Update war file
bash "update1_DUUserReg.war" do
	cwd "/tmp/#{release_file}"
	code <<-EOH
		/usr/bin/jar -uf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/web.xml >> /tmp/pub 2>&1
		/usr/bin/jar -uf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/userreg-services.xml >> /tmp/pub 2>&1
		/usr/bin/jar -uf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/resources/application.properties >> /tmp/pub 2>&1
		/usr/bin/jar -uf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/resources/messages.properties >> /tmp/pub 2>&1
		/usr/bin/jar -uf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/resources/messages_ar.properties >> /tmp/pub 2>&1
	EOH
end

