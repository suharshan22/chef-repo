#
# Cookbook Name:: bo-oms
# Recipe:: report_server
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

release_file = node['backoffice']['report_server']['filename']

# Download war file
remote_file "#{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file}" do
  source "#{node['package_url']}/#{release_file}"
	action :create_if_missing
end

tmp_dir = "/tmp/#{release_file}/WEB-INF/resources"

# Create temp directory
directory tmp_dir do
	recursive true
	action :create
end

# Create jdbc.propertie
template "#{tmp_dir}/jdbc.properties" do
	source "report_server/jdbc.properties.erb"
	variables ({
		:db_host => node['bo-oms']['report_server']['db_host'],
		:db_sid => node['bo-oms']['report_server']['db_sid']
	})
end

# Construct reportServer.properties
properties = data_bag_item('report_server', 'properties')

properties_content = ''
properties.each do |key,value|
	if key == 'id' 
		next
	end

	properties_content << "#{key}=#{value}\n"
end

file "#{tmp_dir}/reportServer.properties" do
	content properties_content
end

# Update war file
execute "update_jdbc.properties" do
	cwd "/tmp/#{release_file}"
	command "/usr/bin/jar -uvf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/resources/jdbc.properties >> /tmp/pub 2>&1"
 	action :run
end

execute "update_reportServer.properties" do
	cwd "/tmp/#{release_file}"
	command "/usr/bin/jar -uvf #{node['jboss-eap']['jboss_home']}/server/default/deploy/#{release_file} WEB-INF/resources/reportServer.properties >> /tmp/pub 2>&1"
 	action :run
end

# Issue permissions and ownership change
execute "change_report_server_permission" do
  command "chown -Rf directfn:directfn #{node['jboss-eap']['jboss_home']}"
  command "chmod -Rf 777 #{node['jboss-eap']['jboss_home']}"
  action :run
end
