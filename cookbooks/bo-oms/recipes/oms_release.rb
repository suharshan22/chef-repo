#
# Cookbook Name:: bo-oms
# Recipe:: oms-release
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

# Copy OMS release files to Jboss
data_bag("bo_oms_releases")

release_files = data_bag_item('bo_oms_releases', node['bo_oms']['release'])

release_files['files'].each do |file|
  remote_file "#{node['jboss-eap']['jboss_home']}/server/default/deploy/#{file}" do
    source "#{node['package_url']}/#{file}"
  end
end
