# Cookbook Name:: bo_oms
# Recipe:: jboss
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

node.override['jboss-eap']['version'] = "6.1.0"
node.override['jboss-eap']['install_path'] = node['app_home']
node.override['jboss-eap']['admin_user'] = "youradmin"
node.override['jboss-eap']['admin_passwd'] = "ZYxalFHy-7A" # Note the password has to be >= 8 characters, one numeric, one special
node.override['jboss-eap']['start_on_boot'] = true
node.override['jboss-eap']['config_dir'] = "#{node['app_home']}/jboss/server/default/conf"
node.override['jboss-eap']['log_dir'] = "#{node['app_home']}/jboss/server/default/log"
node.override['jboss-eap']['package_url'] = "#{node['package_url']}/jboss-as-distribution-6.1.0.Final.zip"
node.override['jboss-eap']['checksum'] = '3426fda6b7066027437de76d02dd5418abc614aec3e21255f8aa704e56a1a291'
node.override['jboss-eap']['jboss_user'] = 'directfn'
node.override['jboss-eap']['jboss_group'] = 'directfn'
node.override['jboss-eap']['jboss_home'] = "#{node['app_home']}/#{node['jboss-eap']['symlink']}-#{node['jboss-eap']['version']}"
include_recipe "jboss-eap::jboss-as"
