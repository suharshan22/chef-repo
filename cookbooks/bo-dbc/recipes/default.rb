#
# Cookbook Name:: bo-dbc
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

dbc_home = "#{node['app_home']}/dbc"

# Download package
remote_file "/tmp/#{node['bo_dbc']['filename']}" do
  source "#{node['package_url']}/#{node['bo_dbc']['filename']}"
end

directory dbc_home do
  mode "0755"
  action :create
end

# Extract archive
execute "extract_archive" do
  command "tar -zxf /tmp/#{node['bo_dbc']['filename']} -C #{dbc_home}"
end

# Populate Settings.xml from template
template "#{dbc_home}/config/Settings.xml" do
	source "Settings.xml.erb"
	variables ({
		:db_server => node['bo_dbc']['ora_server'],
		:db_name => node['bo_dbc']['ora_sid']
	})
end

# Issue permissions and ownership change
execute "change_dbc_permission" do
  command "chown -Rf directfn:directfn #{dbc_home}/; chmod -Rf 777 #{dbc_home}/"
  action :run
end

execute "start_dbc" do
	cwd dbc_home
	command "/bin/bash start.sh"
	user "directfn"
	action :run
end
