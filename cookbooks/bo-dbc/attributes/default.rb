default['bo_dbc']['min_memory_in_mb'] = 1024
default['bo_dbc']['max_memory_in_mb'] = 2048
default['bo_dbc']['ora_server'] = 'mfsbo_101.mfsnet.io'
default['bo_dbc']['ora_sid'] = 'OMSBO'
default['bo_dbc']['app_home'] = '/home/directfn/app'
default['bo_dbc']['filename'] = 'DBC.tar.gz'
