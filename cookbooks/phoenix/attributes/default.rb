default['phoenix']['has_memory_limit'] = true
default['phoenix']['min_memory_in_mb'] = 512
default['phoenix']['max_memory_in_mb'] = 512
default['phoenix']['home'] = "/home/directfn/phoenix"
default['phoenix']['git_url'] = "ssh://git@altssh.bitbucket.org:443/mfsprice/phoenix.git"
#default['phoenix']['tag'] = "master"
default['phoenix']['jgroups_protocol'] = 'mcast'
default['phoenix']['mcast_addr'] = '224.0.0.1'
