name             'phoenix'
maintainer       'DirectFN'
maintainer_email 'p.pubudu@directfn.com'
license          'All rights reserved'
description      'Installs/Configures phoenix'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends "java"
depends "git"
depends "mysql"
depends "database"
depends 'mysql2_chef_gem', '~> 1.0'
depends "ssh_known_hosts"
depends "s3_file"
