#
# Cookbook Name:: phoenix
# Recipe:: update
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

include_recipe "git"

#execute "kill_phoenix" do
#	command "kill -9 `ps -eaf | grep java | grep ConnectionManager | awk -F ' ' '{print $2}'` 2> /dev/null"
#	returns 0
#end

bash 'kill_phoenix' do
	code "kill -9 `ps -eaf | grep java | grep ConnectionManager | awk -F ' ' '{print $2}'` 2> /dev/null"
	returns [0, 1]
end

execute "update_git" do
	cwd node["phoenix"]["home"]
	command "git fetch; git checkout tags/#{node["phoenix"]["tag"]}"
end

#execute "change_permission" do
#  command "chown -Rf directfn:directfn #{node['phoenix']['home']}"
#  action :run
#end

#execute "start_phoenix" do
#	command "/bin/bash #{node['phoenix']['home']}/start"
#end
