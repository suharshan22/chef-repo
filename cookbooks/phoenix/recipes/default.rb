#
# Cookbook Name:: phoenix
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#
include_recipe "java"
include_recipe "git"
include_recipe "phoenix::directfn"

# Clone Phonenix from GIT
git node["phoenix"]["home"] do
	repository node['phoenix']['git_url']
	revision node["phoenix"]["tag"]
	action :sync
end

#execute 'checkout_to_tag' do
#	cwd node["phoenix"]["home"]
#	command "git checkout tags/#{node["phoenix"]["tag"]}"
#end

if node.chef_environment == 'production'
# Configure run script
memory_limit = ( node["phoenix"]["has_memory_limit"]) ? "-Xmx#{node["phoenix"]["min_memory_in_mb"]}m -Xms#{node["phoenix"]["max_memory_in_mb"]}m" : ""

template "#{node["phoenix"]["home"]}/run" do
	source "run.erb"
	mode "0755"
	owner "root"
	group "root"
	variables ({
		:memory_limit => memory_limit
	})
end

# Load all connections from databag
connections = data_bag("phoenix_connections")

# Iterate over each connection
conn_content = ''
connections.each do |conn_item|
	conn = data_bag_item("phoenix_connections", conn_item)
	
	all_content = "[CONNECTION]\n"

	# Iterate over each key/value pair
	conn.each do |key, value|

		# Skip the 'id' key
		if key == "id"
			next
		end

		# Append key/value pairs to content
		all_content << <<-"EOF"
		#{key}=#{value}
		EOF
	end

	all_content << "\n\n"
	conn_content << all_content

end

# Configure config.ini file
template "#{node["phoenix"]["home"]}/config/config.ini" do
	source "config.ini.erb"
	mode "0744"
	variables ({
		:server_id => node["phoenix"]["server_id"],
		:connections => conn_content
	})
end

# Configure infinispan cluster
template "#{node["phoenix"]["home"]}/config/infinispan.xml" do
	source "infinispan.xml.erb"
	variables ({
		:jgroups_config_file => 'jgroups.xml'
	})
end

# Populate relevant template according to protocol
case node['phoenix']['jgroups_protocol']
when 's3_ping'
	template "#{node["phoenix"]["home"]}/config/jgroups.xml" do
		source "jgroups-ec2.xml.erb"
		variables ({
			:private_ip => node['ipaddress'],
			:access_key => node['phoenix']['s3']['access_key'],
			:secret_access_key => node['phoenix']['s3']['secret_access_key'],
			:location => node['phoenix']['s3']['location']	
		})
	end 
when 'mcast'
  template "#{node["phoenix"]["home"]}/config/jgroups.xml" do
    source "jgroups-mcast.xml.erb"
    variables ({
      :mcast_addr => node['phoenix']['mcast_addr']
    })
  end 
end

#mysql_phoenix_password = (node[:phoenix][:mysql].attribute?(:password))? node['phoenix']['mysql']['password'] : (0...8).map { (65 + rand(26)).chr }.join

if !node[:phoenix][:mysql].attribute?(:password) 
	node.default['phoenix']['mysql']['password'] = (0...8).map { (65 + rand(26)).chr }.join	
	node.default['phoenix']['mysql']['host'] = node['ipaddress']
end

# Populate persistence.xml 
# Deploy Mysql
if node.role?('phoenix_with_mysql')
	include_recipe "phoenix::mysql"
end

template "#{node["phoenix"]["home"]}/infinispan/META-INF/persistence.xml" do
  source "persistence.xml.erb"
  mode "0755"
  variables ({
    :mysql_host => node['phoenix']['mysql']['host'],
    :mysql_dbname => node['phoenix']['mysql']['dbname'],
    :mysql_dbuser => node['phoenix']['mysql']['user'],
    :mysql_dbpassword => node['phoenix']['mysql']['password']
  })
end
end

execute "change_permission" do
  command "chown -Rf directfn:directfn #{node['phoenix']['home']}"
  action :run
end

# Run phoenix
#execute "run_phoenix" do
#	cwd node["phoenix"]["home"]
#	command "#{node["phoenix"]["home"]}/run"
#	action :run
#end

# TODO: temporary fix; Need to open relevant ports instead of stopping the whole thing
execute "stop_iptables" do
	command "/etc/init.d/iptables stop"
end

#if node[:phoenix][:mysql].attribute?(:password)
#	mysql_phoenix_password
#end

