#
# Cookbook Name:: phoenix
# Recipe:: simple
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#
include_recipe "java"
include_recipe "git"
#include_recipe "iptables"
include_recipe "phoenix::directfn"
require 'json'

# Clone Phonenix from GIT
git node["phoenix"]["home"] do
  repository node['phoenix']['git_url']
  revision node["phoenix"]["tag"]
  action :sync
end

execute 'checkout_to_tag' do
	cwd node["phoenix"]["home"]
	command "git checkout tags/#{node["phoenix"]["tag"]}"
end

execute "change_permission" do
  command "chown -Rf directfn:directfn #{node['phoenix']['home']}"
  action :run
end

# Add iptables rules
#iptables_rule '8000' do
#	action :enable
#end
#
#iptables_rule '19006' do
#	action :enable
#end
#
#iptables_rule '9090' do
#	action :enable
#end

# Encrypted password for directfn user
#password = (0...8).map { (65 + rand(26)).chr }.join
#salt = rand(36**8).to_s(36)
#shadow_hash = password.crypt("$6$" + salt)
#
#metadata = Hash.new
#metadata[:directfn_passwd] = password
#
## Set password for directfn user
#user "directfn" do
#  password shadow_hash
#  action :modify
#end
#
## Create meta.json file
#file "#{node['phoenix']['home']}/meta.json" do
#	content metadata.to_json
#end

# Run phoenix
#execute "run_phoenix" do
# cwd node["phoenix"]["home"]
# command "#{node["phoenix"]["home"]}/run"
# action :run
#end
