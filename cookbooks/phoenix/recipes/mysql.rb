#
# Cookbook Name:: phoenix
# Recipe:: mysql
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#
#	This recipe includes Phoenix's all mysql related operations
#

require 'json'

mysql_root_password = (0...8).map { (65 + rand(26)).chr }.join
# Install Mysql
mysql_service 'foo' do
  port '3306'
  version '5.5'
  action [:create]
end

# Start Mysql
execute "start_mysql" do
	command "/etc/init.d/mysqld start"
	action :run
end

# Generate random password and assign the root user
execute "set_mysql_admin_password" do
	command "/usr/bin/mysqladmin -u root password #{mysql_root_password}"
end

# Install mysql2 ruby gem 
mysql2_chef_gem 'default' do
  action :install
end

mysql_connection_info = {
	:host => '127.0.0.1',
	:Username => 'root',
	:password => mysql_root_password
}

# Create phoenix database
mysql_database 'phoenix' do
	connection mysql_connection_info
	action :create
end

# Create phoenix user
mysql_database_user 'phoenix_user' do
	connection mysql_connection_info
	password node['phoenix']['mysql']['password']
	host '%'
	action :grant
end

# Write Mysql root password to meta.json
metadata = Hash.new
metadata[:mysql_root_password] = mysql_root_password
metadata[:mysql_phoenix_password] = node['phoenix']['mysql']['password']

file "#{node["phoenix"]["home"]}/meta.json" do
	content metadata.to_json
end

## Populate persistence.xml 
#template "#{node["phoenix"]["home"]}/infinispan/META-INF/persistence.xml" do
#	source "persistence.xml.erb"
#	mode "0755"
#	variables ({
#		:mysql_host => node['phoenix']['mysql']['host'],
#		:mysql_dbname => node['phoenix']['mysql']['dbname'],
#		:mysql_dbuser => node['phoenix']['mysql']['user'],
#		:mysql_dbpassword => mysql_phoenix_password
#	})	
#end

