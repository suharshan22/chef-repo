require 'digest/sha2'

#TODO : add <private ip> <hostname> entry to hosts file

# Encrypted password for directfn user
#password = (0...8).map { (65 + rand(26)).chr }.join
#salt = rand(36**8).to_s(36)
#shadow_hash = password.crypt("$6$" + salt)
#
## Create directfn user
#user "directfn" do
#  supports :manage_home => true
#  home "/home/directfn"
#  shell "/bin/bash"
#  action :create
#end

# Create app directory
directory "/home/directfn/app" do
  owner "directfn"
  group "directfn"
  mode "0777"
  action :create
end

# Create .ssh directory for directfn user
directory "/home/directfn/.ssh" do
  owner "directfn"
  group "directfn"
  action :create
end

cookbook_file "clouduser" do
  path "/home/directfn/.ssh/id_rsa"
  owner "directfn"
  group "directfn"
  mode "0400"
  action :create_if_missing
end

# Add git host to known_hosts
ssh_known_hosts_entry 'bitbucket.org'
ssh_known_hosts_entry 'altssh.bitbucket.org'
