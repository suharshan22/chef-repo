#
# Cookbook Name:: mfs-sms-manager
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

sms_home = "#{node['app_home']}/sms-manager"

# Download tar file
remote_file "tmp/#{node['mfs-sms-manager']['filename']}" do
	source "#{node['package_url']}/#{node['mfs-sms-manager']['filename']}"
end

directory sms_home do
	mode "0755"
	action :create
end

# Extract sms manager
execute "extract_sms_manager" do
	command "tar -zxf tmp/#{node['mfs-sms-manager']['filename']} -C #{sms_home}"
end

# Create spring.config.xml file
template "#{sms_home}/settings/spring.config.xml" do
	source "spring.config.xml.erb"
	variables ({
		:attributes => node['mfs-sms-manager']['routing_rules'],
		:connectors => node['mfs-sms-manager']['gateway_connectors'],
		:connector_arr => node['mfs-sms-manager']['connectors'],
		:secureips => node['mfs-sms-manager']['secureips'],
		:useraccounts => node['mfs-sms-manager']['useraccountmap']
	})
end

# Create sms_gateway.propertie file
template "#{sms_home}/settings/sms_gateway.properties" do
	source "sms_gateway.properties.erb"
	variables ({
		:mfs_db_host => node['mfs-sms-manager']['mfs_db_host'],
		:mfs_db_sid => node['mfs-sms-manager']['mfs_db_sid'],
		:dbfs_db_host => node['mfs-sms-manager']['dbfs_db_host'],
		:dbfs_db_sid => node['mfs-sms-manager']['dbfs_db_sid']
	})
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{sms_home}; chmod -Rf 777 #{sms_home}"
  action :run
end

# Run sms-manager
#execute "run_sms_manager" do
# command "#{sms_home}/start-service.sh"
# action :run
#end
