default['mfs-sms-manager']['filename'] = 'sms_manager_build-1.0.1.11.tgz'
default['mfs-sms-manager']['mfs_db_host'] = 'mfsbo_101.mfsnet.io'
default['mfs-sms-manager']['mfs_db_sid'] = 'OMSBO'
default['mfs-sms-manager']['dbfs_db_host'] = 'mfsbo_101.mfsnet.io'
default['mfs-sms-manager']['dbfs_db_sid'] = 'OMSDBFS'
default['mfs-sms-manager']['routing_rules'] = [
	'00971551061920\d{0}+,GateWayConnector-ES',
	'971551061920\d{0}+,GateWayConnector-ES',
	'00966500253888\d{0}+,GateWayConnector-ES',
	'966500253888\d{0}+,GateWayConnector-ES',
	'00966554505521\d{0}+,GateWayConnector-ES',
	'966554505521\d{0}+,GateWayConnector-ES',
	'50\d{7}+,GateWayConnector-ES',
	'56\d{7}+,GateWayConnector-ES',
	'050\d{7}+,GateWayConnector-ES',
	'056\d{7}+,GateWayConnector-ES',
	'0097150\d{7}+,GateWayConnector-ES',
	'97150\d{7}+,GateWayConnector-ES',
	'97156\d{7}+,GateWayConnector-ES',
	'0097156\d{7}+,GateWayConnector-ES',
	'52\d{7}+,GateWayConnector-ES',
	'55\d{7}+,GateWayConnector-ES',
	'052\d{7}+,GateWayConnector-ES',
	'055\d{7}+,GateWayConnector-ES',
	'0097152\d{7}+,GateWayConnector-ES',
	'97155\d{7}+,GateWayConnector-ES',
	'97152\d{7}+,GateWayConnector-ES',
	'0097155\d{7}+,GateWayConnector-ES'
]
default['mfs-sms-manager']['gateway_connectors'] = [
	'GateWayConnector-HSL',
	'GateWayConnector-Rajhi'
]
