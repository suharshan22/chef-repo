#
# Cookbook Name:: db_sync_validator
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

release_file = node['db_sync_validator']['package_name']
tmp_dir = "/tmp/#{release_file}/WEB-INF"
db_sync_home = "#{node['app_home']}/#{node['db_sync_validator']['app_name']}"

# Download tar file
remote_file "/tmp/db_sync_validator.tgz" do
  source "#{node['package_url']}/#{release_file}"
end

directory "#{db_sync_home}" do
  mode "0755"
  action :create
end

# Create temp directory
directory tmp_dir do
  recursive true
  action :create
end

# Extract package
execute "extract_package" do
  command "tar -zxf /tmp/db_sync_validator.tgz -C #{db_sync_home}"
end

# Create DBSyncValidator.properties
template "#{db_sync_home}/settings/DBSyncValidator.properties" do
	source "DBSyncValidator.properties.erb"
	variables ({
		:private_ip => node['ipaddress'],
		:fo_db_host => node['db_sync_validator']['fo_db_host'],
		:fo_db_sid => node['db_sync_validator']['fo_db_sid'],
		:bo_private_ip => node['db_sync_validator']['bo_private_ip'],
		:fo_private_ip => node['db_sync_validator']['fo_private_ip']
	})
end

# Create mvc-dispatcher-servlet.xml
template "#{tmp_dir}/mvc-dispatcher-servlet.xml" do
	source "mvc-dispatcher-servlet.xml.erb"
	variables ({
		:useraccounts => node['db_sync_validator']['accountmap']
	})
end

# Update war file
execute "update_db_sync_webapp" do
	cwd "/tmp/#{release_file}"
	command "/usr/bin/jar -uvf #{db_sync_home}/lib/#{node['db_sync_validator']['webapp_filename']} WEB-INF/mvc-dispatcher-servlet.xml >> /tmp/pub 2>&1"
	action :run
end

# Issue permissions and ownership change
execute "change_permission" do
  command "chown -Rf directfn:directfn #{db_sync_home}"
  command "chmod -Rf 777 #{db_sync_home}"
  action :run
end

# Start service
execute "start_db_sync_validator" do
	command "#{db_sync_home}/run.sh"
end
