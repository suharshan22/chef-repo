name             'db_sync_validator'
maintainer       'DirectFN'
maintainer_email 'p.pubudu@directfn.com'
license          'All rights reserved'
description      'Installs/Configures db_sync_validator'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
