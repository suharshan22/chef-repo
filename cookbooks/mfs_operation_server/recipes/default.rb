#
# Cookbook Name:: mfs_operation_server
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

os_home = "#{node['app_home']}/operation_server"

# Download package
remote_file "/tmp/#{node['op_server']['filename']}" do
  source "#{node['package_url']}/#{node['op_server']['filename']}"
end

directory os_home do
  mode "0755"
  action :create
end

# Extract archive
execute "extract_archive" do
  command "tar -zxf /tmp/#{node['op_server']['filename']} -C #{os_home}"
end

# Create start.sh
template "#{os_home}/start.sh" do
	source "start.sh.erb"
	mode "0755"
	variables ({
		:java_home => node['java7_home'],
		:memory => node['op_server']['memory']
	})
end

# Create os.properties
template "#{os_home}/settings/os.properties" do
	source "os.properties.erb"
	variables ({
		:sms_sender_ip => node['op_server']['sms_sender_ip'],
		:oms_db_host => node['op_server']['oms_db_host'],
		:oms_db_sid => node['op_server']['oms_db_sid'],
		:oms_db_username_enc => node['op_server']['oms_db_username_enc'],
		:oms_db_password_enc => node['op_server']['oms_db_password_enc']
	})
end

# Create SnapShot_Update_Config.properties
template "#{os_home}/jobs/config/SnapShot_Update_Config.properties" do
	source "SnapShot_Update_Config.properties.erb"
	variables ({
		:price_db_host => node['op_server']['price_db_host'],
		:price_db_sid => node['op_server']['price_db_sid'],
		:price_db_user => node['op_server']['price_db_user'],
		:price_db_pass => node['op_server']['price_db_pass'],
		:email_db_host => node['op_server']['email_db_host'],
		:email_db_sid => node['op_server']['email_db_sid'],
		:email_db_user => node['op_server']['email_db_user'],
		:email_db_pass => node['op_server']['email_db_pass'],
		:oms_version => node['op_server']['oms_version']
	})
end

# Create job-schedule-config.xml
cookbook_file "#{os_home}/jobs/config/job-schedule-config.xml" do
	source "job-schedule-config.xml"
end

# Iterate through each job attribute and create files
site_attr = node['op_server']['site_name']
node['op_server']["#{site_attr}"].each do |dir, file|

	# Create directory
	if dir != 'default' 
		directory "#{os_home}/jobs/config/#{dir}" do
			recursive true
			action :create
		end	
		dir_name = "#{os_home}/jobs/config/#{dir}"
	else
		dir_name = "#{os_home}/jobs/config"
	end

	# Create template file
	template "/tmp/#{dir}" do
		source "#{dir}.erb"
		variables ({
			:web_service_ip => node['op_server']['web_service_ip'],
			:web_service_name => node['op_server']['web_service_name'],
			:db_host => node['op_server']['db_host'],
			:db_sid => node['op_server']['db_sid'],
			:dbuser => node['op_server']['dbuser'],
			:dbpass => node['op_server']['dbpass']
		})
	end

	# Loop through directory
	node['op_server'][site_attr][dir].each do |file, value|

		# Collect file content
		#file_content = "<%= render '/tmp/#{dir}' %>"
		file_content = ''
		node['op_server'][site_attr][dir][file].each do |key, value|
			file_content << "#{key}=#{value}\n"
		end

		# Create file
		file "#{dir_name}/#{file}" do
			content file_content
			action :create
			#notifies :run, "execute[append_template_#{file}]", :immediately
		end

		# Append template
		execute "append_template_#{file}" do
			command "cat /tmp/#{dir} >> #{dir_name}/#{file}"
			#action :nothing
		end

	end

end
