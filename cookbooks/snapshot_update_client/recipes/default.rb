#
# Cookbook Name:: snapshot_update_client
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

suc_home = "#{node['app_home']}/snapshot_update_client"

# Download tar file
remote_file "/tmp/#{node['snapshot_update_client']['filename']}" do
  source "#{node['package_url']}/#{node['snapshot_update_client']['filename']}"
end

directory suc_home do
  mode "0755"
  action :create
end

# Extract application
execute "extract_snapshot_update_client" do
  command "tar -zxf /tmp/#{node['snapshot_update_client']['filename']} -C #{suc_home}"
end

# Create config.ini file
template "#{suc_home}/config/config.ini" do
	source "config.ini.erb"
	variables ({
		:price_db_host => node['snapshot_update_client']['price_db_host'],
		:price_db_sid => node['snapshot_update_client']['price_db_sid'],
		:price_db_username => node['snapshot_update_client']['price_db_username'],
		:price_db_password => node['snapshot_update_client']['price_db_password'],
		:exchanges => node['snapshot_update_client']['exchanges'].join(",")
	})
end

# Create start.sh
template "#{suc_home}/start.sh" do
	source "start.sh.erb"
	mode "0755"
	variables ({
		:suc_home => suc_home,
		:java_path => node['snapshot_update_client']['java_path'],
		:memory_limit => node['snapshot_update_client']['memory_limit']
	})
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{suc_home}; chmod -Rf 777 #{suc_home}"
  action :run
end

# start service
#execute "start_service" do
#	command "#{suc_home}/start.sh"
#end
