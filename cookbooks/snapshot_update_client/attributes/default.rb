default['snapshot_update_client']['home'] = '/home/directfn/app'
default['snapshot_update_client']['java_path'] = '/usr/java/jdk1.7.0'
default['snapshot_update_client']['filename'] = 'snapshot_update_client.tgz'
default['snapshot_update_client']['price_db_host'] = '172.17.240.198'
default['snapshot_update_client']['price_db_sid'] = 'MFSNGEN'
default['snapshot_update_client']['price_db_username'] = 'MDC_MFS_OMS'
default['snapshot_update_client']['price_db_password'] = 'password'
default['snapshot_update_client']['exchanges'] = [
	'NSDQ',
	'AMEX',
	'OPRA',
	'DFM',
	'ADSM',
	'TDWL'
]
default['snapshot_update_client']['memory_limit'] = '512'
