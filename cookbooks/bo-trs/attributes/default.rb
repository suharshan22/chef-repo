default['bo_trs']['release'] = '114'
default['bo_trs']['min_memory_in_mb'] = 1024
default['bo_trs']['max_memory_in_mb'] = 2048
default['bo_trs']['ora_server'] = 'mfsbo_101.mfsnet.io'
default['bo_trs']['ora_sid'] = 'OMSBO'
default['bo_trs']['java_path'] = '/usr/java'
default['bo_trs']['filename'] = 'TRS.tar.gz'
