name             'bo-trs'
maintainer       'DirectFN'
maintainer_email 'p.pubudu@directfn.com'
license          'All rights reserved'
description      'Installs/Configures bo-trs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
