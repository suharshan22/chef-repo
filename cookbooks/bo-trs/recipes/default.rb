#
# Cookbook Name:: bo-trs
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

# Wait 5 minutes until OMS gets started
bash "is_port_open" do
  code <<-EOH
  nc -z #{node['ipaddress']} 1099
  EOH
  retries 60
  retry_delay 10
end

class Chef::Recipe
  include ChefTRS
end

trs_home = "#{node['app_home']}/trs"

# Get release files from data bag
release_files = data_bag_item('bo_trs_releases', node['bo_trs']['release'])
remote_file "/tmp/#{node['bo_trs']['filename']}" do
	source "#{node['package_url']}/#{node['bo_trs']['filename']}" 
end

directory trs_home do
  mode "0755"
  action :create
end

# Extract TRS 
execute "extract_archive" do
  command "tar -zxf /tmp/#{node['bo_trs']['filename']} -C #{trs_home}"
end

# Populate settings.ini from template
template "#{trs_home}/config/settings.ini" do
	source "settings.ini.erb"
	variables ({
		:private_ip => node['ipaddress']
	})	
end

# Populate start.sh from template
template "#{trs_home}/start.sh" do
	source "start.sh.erb"
	variables ({
		:release_files => data_bag_item('bo_trs_releases', node['bo_trs']['release'])
	})
end

# Issue permissions and ownership change
execute "change_trs_permission" do
	command "chown -Rf directfn:directfn #{trs_home}"
	command "chmod -Rf 777 #{trs_home}"
	action :run
end

# TODO: Need to check if oms is running
oms_started=false
ruby_block 'chef_if_oms_is_up' do
	block do	
		oms_started=ChefTRS.is_port_open?('127.0.0.1', '22')
	end
	action :run
end

file '/tmp/pub' do
	content oms_started.to_s
end

# Start TRS service
execute "start_trs" do
	cwd trs_home
	command "/bin/bash start.sh"
	user "directfn"
	action :run
end

