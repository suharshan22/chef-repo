default['fo_trs']['release'] = '1.0.38'
default['fo_trs']['min_memory_in_mb'] = 1024
default['fo_trs']['max_memory_in_mb'] = 2048
default['fo_trs']['ora_server'] = 'mfsfo_101.mfsnet.io'
default['fo_trs']['ora_sid'] = 'OMSFO'
default['fo_trs']['ora_username'] = 'MDC_MFS_OMS'
default['fo_trs']['ora_password'] = 'password'
default['fo_trs']['filename'] = 'FO-TRS.tgz'
