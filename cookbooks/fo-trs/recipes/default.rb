#
# Cookbook Name:: fo-trs
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

# TODO: Exit if OMS port not open

trs_home = "#{node['app_home']}/trs"

# Wait 5 minutes until OMS gets started
bash "is_port_open" do
  code <<-EOH
  nc -z #{node['ipaddress']} 1099
  EOH
  retries 60
  retry_delay 10
end

# Download TRS from remote location
remote_file "/tmp/#{node['fo_trs']['filename']}" do
  source "#{node['package_url']}/#{node['fo_trs']['filename']}"
end

directory trs_home do
  mode "0755"
  action :create
end

# Extract TRS 
execute "extract_trs" do
  command "tar -zxf /tmp/#{node['fo_trs']['filename']} -C #{trs_home}"
end

# Create settings.ini file
template "#{trs_home}/config/settings.ini" do
  source "settings.ini.erb"
		variables ({
			:fo_oms_private_ip => node['ipaddress'],
			:bo_oms_private_ip => node['bo_oms']['private_ip'],
			:ora_server => node['fo_trs']['ora_server'],
			:ora_sid => node['fo_trs']['ora_sid'],
			:ora_username => node['fo_trs']['ora_username'],
			:ora_password => node['fo_trs']['ora_password']
		})
end

# TODO: Set TRS console user data

# Create start-service.sh
template "#{trs_home}/start-service.sh" do
	source "start-service.sh.erb"
	mode "0755"
	variables ({
    :java_home => node['java7_home'],
    :min_memory_in_mb => node['fo_trs']['min_memory_in_mb'],
    :max_memory_in_mb => node['fo_trs']['max_memory_in_mb']
	})
end 

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{trs_home}; chmod -Rf 777 #{trs_home}"
  action :run
end

# Start TRS
execute "start_trs" do
	user 'directfn'
	command "#{trs_home}/start-service.sh"
end
