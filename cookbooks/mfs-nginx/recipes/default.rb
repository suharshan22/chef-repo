#
# Cookbook Name:: mfs-nginx
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'nginx'

# Get conf file list to be deployed
config_list = node['mfs-nginx']['list']

directory "#{node['nginx']['dir']}/includes" do
	action :create
end

directory "#{node['nginx']['dir']}/certificates" do
	action :create
end

# Copy certificates
cert_list = node['mfs-nginx']['certificates']
cert_list.each do |cert|
	cookbook_file "#{node['nginx']['dir']}/certificates/#{cert}" do
		source cert
	end
end

# Iterate through each file and fill in variables
config_list.each do |config|
	params = node['mfs-nginx'][config]

	filepath = ( config.include? '.data' ) ? "#{node['nginx']['dir']}/includes/#{config}" : "#{node['nginx']['dir']}/sites-available/#{config}"
	template filepath do
		source "#{config}.erb"
		variables (params)
	end

	# Enable vhost
	if !config.include? '.data'
		execute "enable_site" do
			command "/usr/sbin/nxensite #{config}"
		end
	end
end

# Restart nginx service
service "nginx" do
	action :restart
end
