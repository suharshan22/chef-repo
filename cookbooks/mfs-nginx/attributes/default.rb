default['mfs-nginx']['vas_url'] = 'vas.dutrade.com'
default['mfs-nginx']['data_url'] = 'data.directfn.com'
default['mfs-nginx']['ldc_url'] = 'dc-uat.directfn.com'
default['mfs-nginx']['proxy_number'] = '2'

default['mfs-nginx']['certificates'] = [
'gtnsecure.dutrade.com.crt',
'gtnsecure.dutrade.com.key',
'mbs.mubashertrade.com.crt',
'mbs.mubashertrade.com.key',
'rs.dutrade.com.crt',
'rs.dutrade.com.key',
'secure.mubashertrade.com.crt',
'secure.mubashertrade.com.key',
'secure2.mubashertrade.com.crt',
'secure2.mubashertrade.com.key',
'trade.dutrade.com.crt',
'trade.dutrade.com.key',
'zaitrading.dutrade.com.crt',
'zaitrading.dutrade.com.key'
]

default['mfs-nginx']['list'] = [
'global2.mubashertrade.com.conf', 
'global2.mubashertrade.com.data',
'secure2.mubashertrade.com.data',
'secure2.mubashertrade.com.conf'
]
