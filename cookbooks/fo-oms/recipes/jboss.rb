# Cookbook Name:: fo-oms
# Recipe:: jboss
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#
node.override['jboss-eap']['version'] = '6.2.0'
node.override['jboss-eap']['config_dir'] = "/etc/jboss-as"
node.override['jboss-eap']['log_dir'] = "/var/log/jboss"
node.override['jboss-eap']['package_url'] = "#{node['package_url']}/jboss-eap-6.2.0.zip"
#node.override['jboss-eap']['package_url'] = "https://s3.amazonaws.com/pub22/jboss-eap-6.2.0.zip"
#node.override['jboss-eap']['package_url'] = "#{node['package_url']}/jboss-as-distribution-6.1.0.Final.zip"
node.override['jboss-eap']['checksum'] = '627773f1798623eb599bbf7d39567f60941a706dc971c17f5232ffad028bc6f4'
node.override['jboss-eap']['admin_user'] = "youradmin"
node.override['jboss-eap']['admin_passwd'] = "ZYxalFHy-7A" # Note the password has to be >= 8 characters, one numeric, one special
node.override['jboss-eap']['start_on_boot'] = true
node.override['jboss-eap']['install_path'] = node['app_home']
node.override['jboss-eap']['jboss_home'] = "#{node['app_home']}/#{node['jboss-eap']['symlink']}-#{node['jboss-eap']['version']}"
#node.override['jboss-eap']['config_dir'] = "#{node['app_home']}/jboss/server/default/conf"
#node.override['jboss-eap']['log_dir'] = "#{node['app_home']}/jboss/server/default/log"
node.override['jboss-eap']['jboss_user'] = 'directfn'
node.override['jboss-eap']['jboss_group'] = 'directfn'
include_recipe "jboss-eap"
