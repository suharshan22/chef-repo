#
# Cookbook Name:: fo-oms
# Recipe:: default
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

include_recipe "git"
include_recipe "fo-oms::jboss"
include_recipe "ssh_known_hosts"

# Add git host to known_hosts
ssh_known_hosts_entry node['fo_oms']['git']['url']

directory node['jboss-eap']['jboss_home'] do
  owner "directfn"
  group "directfn"
  recursive true
end

# DirectFN specific one time configurations to jboss
unless Dir.exist? "#{node['jboss-eap']['jboss_home']}/.git"
bash "merge_jboss_changes" do
  cwd node['jboss-eap']['jboss_home']
  code <<-EOH
  git init
  git remote add origin #{node['fo_oms']['git']['repo_name']}
  git fetch
  #git add .
  #git commit -m 'test'
  git checkout -t origin/master -f
  EOH
  #not_if { ::Dir.exists?("#{node['jboss-eap']['jboss_home']}/.git") }
end
end

# Create standalone.xml file
template "#{node['jboss-eap']['jboss_home']}/standalone/configuration/standalone.xml" do
	source "standalone.xml.erb"
  mode "0755"
	variables ({
    :ora_server => node['fo_oms']['ora_server'],
    :ora_sid => node['fo_oms']['ora_sid'],
		:private_ip => node['ipaddress'],
		:ora_username => node['fo_oms']['ora_username'],
		:ora_password => node['fo_oms']['ora_password']
	})	
end

# Create bin/standalone.conf
template "#{node['jboss-eap']['jboss_home']}/bin/standalone.conf" do
	source "standalone.conf.erb"
	mode "0755"
	variables ({
		:java_home => '/usr/java/latest',
    :min_memory_in_mb => node['fo_oms']['min_memory_in_mb'],
    :max_memory_in_mb => node['fo_oms']['max_memory_in_mb']
	})
end

# Create bin/startnet.sh
template "#{node['jboss-eap']['jboss_home']}/bin/startnet.sh" do
	source "startnet.sh.erb"
	mode "0755"
	variables ({
    :private_ip => node['ipaddress']
	})
end

# Add releae specific files
include_recipe "fo-oms::oms_release"

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{node['jboss-eap']['jboss_home']}; chmod -Rf 777 #{node['jboss-eap']['jboss_home']}"
  action :run
end

# Start OMS
execute "start_oms" do
	user 'directfn'
	cwd "#{node['jboss-eap']['jboss_home']}/bin"
	command "/bin/bash startnet.sh"
	action :run
end
