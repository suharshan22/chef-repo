default['rdbm']['global']['filename'] = 'NGP_SRDBM.tgz'
default['rdbm']['global']['java_path'] = '/usr/java/jdk1.7.0'
default['rdbm']['global']['min_memory_in_mb'] = '1024'
default['rdbm']['global']['max_memory_in_mb'] = '2048'
default['rdbm']['global']['pricedb_host'] = 'mfsfo_101.mfsnet.io'
default['rdbm']['global']['pricedb_sid'] = 'OMSFO'
default['rdbm']['global']['server_ips'] = [
	'172.17.241.200',
	'172.17.241.201'
]
default['rdbm']['global']['server_name'] = 'DEVSCM'
default['rdbm']['global']['fo_oms_ip'] = 'mfsfo_101.mfsnet.io'


