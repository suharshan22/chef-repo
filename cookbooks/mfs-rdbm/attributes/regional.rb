default['rdbm']['regional']['filename'] = 'MubasherRDBM_Regional.tgz'
default['rdbm']['regional']['java_path'] = '/usr/java/jdk1.6.0_31'
default['rdbm']['regional']['min_memory_in_mb'] = '128'
default['rdbm']['regional']['max_memory_in_mb'] = '256'
default['rdbm']['regional']['pricedb_host'] = 'mfsbo_101.mfsnet.io'
default['rdbm']['regional']['pricedb_sid'] = 'OMSBO'
default['rdbm']['regional']['server_ips'] = [
	'172.17.241.186',
	'172.17.241.187'
]
default['rdbm']['regional']['rdbm_id'] = 'MFSUATGCC'


