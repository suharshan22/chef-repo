#
# Cookbook Name:: mfs-rdbm
# Recipe:: rdbm_global
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

rdbm_home = "#{node['app_home']}/rdbm_global"

# Download package
remote_file "/tmp/#{node['rdbm']['global']['filename']}" do
  source "#{node['package_url']}/#{node['rdbm']['global']['filename']}"
end

directory rdbm_home do
	mode "0755"
	action :create
end

# Extract archive
execute "extract_archive" do
	command "tar -zxf /tmp/#{node['rdbm']['global']['filename']} -C #{rdbm_home}"
end

# Create rdbmglobal.sh
template "#{rdbm_home}/rdbmglobal.sh" do
	source "global/rdbmglobal.sh.erb"
	variables ({
		:java_path => node['rdbm']['global']['java_path'],
		:min_memory => node['rdbm']['global']['min_memory_in_mb'],
		:max_memory => node['rdbm']['global']['max_memory_in_mb'],
	})
end

# Create dbmanager.ini
template "#{rdbm_home}/config/dbmanager.ini" do
	source "global/dbmanager.ini.erb"
	variables ({
		:pricedb_host => node['rdbm']['global']['pricedb_host'],
		:pricedb_sid => node['rdbm']['global']['pricedb_sid'],
		:server_ips => node['rdbm']['global']['server_ips'].join(','),
		:server_name => node['rdbm']['global']['server_name'],
		:fo_oms_ip => node['rdbm']['global']['fo_oms_ip']
	})
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{rdbm_home}; chmod -Rf 777 #{rdbm_home}"
  action :run
end

# Run rdbm_global
#execute "run_rdbm_global" do
#	cwd rdbm_home
#	command "#{rdbm_home}/rdbm.sh"
#end
