#
# Cookbook Name:: mfs-rdbm
# Recipe:: rdbm_regional
#
# Copyright 2015, DirectFN
#
# All rights reserved - Do Not Redistribute
#

rdbm_home = "#{node['app_home']}/rdbm_regional"

# Download package
remote_file "/tmp/#{node['rdbm']['regional']['filename']}" do
	source "#{node['package_url']}/#{node['rdbm']['regional']['filename']}"
end

directory rdbm_home do
	mode "0755"
	action :create
end

# Extract archive
execute "extract_archive" do
	command "tar -zxf /tmp/#{node['rdbm']['regional']['filename']} -C #{rdbm_home}"
end

# Create rdbm.sh
template "#{rdbm_home}/rdbm.sh" do
	source "regional/rdbm.sh.erb"
	variables ({
		:rdbm_home => rdbm_home,
		:java_path => node['rdbm']['regional']['java_path'],
		:min_memory => node['rdbm']['regional']['min_memory_in_mb'],
		:max_memory => node['rdbm']['regional']['max_memory_in_mb']
	})
end

# Create dbmanager.ini
template "#{rdbm_home}/config/dbmanager.ini" do
	source "regional/dbmanager.ini.erb"
	variables ({
		:price_db_host => node['rdbm']['regional']['pricedb_host'],
		:price_db_sid => node['rdbm']['regional']['pricedb_sid'],
		:server_ips => node['rdbm']['regional']['server_ips'].join(','),
		:rdbm_id => node['rdbm']['regional']['rdbm_id'],
		:private_ip => node['ipaddress']
	})
end

# Issue permissions and change ownership
execute "change_permission" do
  command "chown -Rf directfn:directfn #{rdbm_home}; chmod -Rf 777 #{rdbm_home}"
  action :run
end

# Run rdbm_regional
#execute "run_rdbm_regional" do
#	cwd rdbm_home
#	command "#{rdbm_home}/rdbm.sh"
#end
